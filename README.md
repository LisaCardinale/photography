# My Passion With Photography
 
I got my own professional camera when I was 15. I was so happy back then, and all I can do was point lens and hit the shutter. I have been into Photography because of my travels. I love capturing the places I have been. The sunset in different places, and the sunrise in the meadows. Photography has been my way to share to everyone the beauty of this world can offer. There is so much to see somewhere there, you just got to pack and go. I am always thankful that I got this talent, and I am glad to show you some of what I got.

![image1.jpg](https://bitbucket.org/repo/4ro5zn/images/4247748402-image1.jpg)

![image2.jpg](https://bitbucket.org/repo/4ro5zn/images/4100953516-image2.jpg)

You can check out [here](http://buyretwits.com) as well if you wish to see more of my work.